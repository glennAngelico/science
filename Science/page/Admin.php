<?php
session_name('user');
session_start();
require('model.php');
if(!isset($_SESSION['user'])){
	header("Location:Login.php");
}
if(isset($_POST['submit'])){
	//echo $_FILES['photo'];
	insertArticle($_POST['categorie'],$_POST['thematic'],$_POST['titre'],$_FILES['photo'],$_POST['auteur'],$_POST['texte'],$_POST['date']);
}
?>
<!doctype html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>contact - Space Science Website Template</title>
	<?php include("concatenation.php"); ?>
</head>
<body>
	<div id="page">
		<?php include('header.php'); ?>
		<div id="body">
			<div class="header">
				<div class="contact">
					<h1>Nouvelle Article</h1>
				
					<form enctype="multipart/form-data" action="Admin.php" method="post">
						<input type="text" name="titre" placeholder="Titre">
						<input name="photo" type="file" placeholder="Photo" />
						
						  <select  name="categorie" class="select" >
							<option value="-1">Categorie</option>
							<?php foreach($listeCat as $element) { ?>
							<option value="<?php echo $element[0]; ?>"><?php echo $element[1]; ?></option>
							<?php } ?>
						  </select>
						   <select  name="thematic" class="select">
							<option value="-1">Thematic</option>
							<?php foreach($listeThem as $element) { ?>
							<option value="<?php echo $element[0]; ?>"><?php echo $element[1]; ?></option>
							<?php } ?>
						  </select>
						
						<input type="text" name="auteur" placeholder="Auteur">
						<input name="date" type="text" placeholder="Date" />
						<textarea type="text" class="ckeditor" name="texte">Contenue de l'article</textarea>
						<input type="submit" name="submit" value="Valider" id="submit">
					</form>
				</div>
			</div>
		</div>
		<?php include('footer.php'); ?>
	</div>
</body>
</html>