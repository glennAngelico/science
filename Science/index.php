<?php
require('page/model.php');
$listeThem=listeThematic("");
$listeCat=listeCategorie("");
$recentPost=listeArticle("  order by daty desc limit 3");
?>
<!doctype html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="Espace News , journal de l'Espace :
		<?php foreach($listeThem as $element) {
			echo $element[1]." ,";
		}?>
		<?php foreach($listeCat as $element) {
			echo $element[1]." ,";
		}?>">
	<title>Espace News , journal de l'Espace : les plus recent et les plus populaire dans le monde du Science
	</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/mobile.css">
	<link rel="stylesheet" href="css/modecss.css" type="text/css">
	<script src="js/mobile.js" type="text/javascript"></script>
</head>
<body>
	<div id="page">
		<div id="header">
			<div>
				<a href="index.php" class="logo"><img src="images/logo.png" alt=""></a>
				<ul id="navigation">
					<li class="selected">
						<a href="index.php">Home</a>
					</li>
					
					<li class="menu">
						<a href="page/thematic-Tout-0.html">Thematic</a>
						<ul class="primary">
						<?php foreach($listeThem as $element) { ?>
							<li>
								<a href="page/thematic-<?php echo $element[1]; ?>-<?php echo $element[0]; ?>.html"><?php echo $element[1]; ?></a>
							</li>
						<?php } ?>
						</ul>
					</li>
					<li class="menu">
						<a href="page/categorie-Tout-0.html">Categorie</a>
						<ul class="secondary">
							<?php foreach($listeCat as $element) { ?>
							<li>
								<a href="page/categorie-<?php echo $element[1]; ?>-<?php echo $element[0]; ?>.html"><?php echo $element[1]; ?></a>
							</li>
							<?php } ?>
						</ul>
					</li>
					
				</ul>
			</div>
		</div>
		<div id="body" class="home">
			<div class="header">
				<div>
					<img src="images/satellite.png" alt="" class="satellite">
					<h1>Espace News</h1>
					<h2>Observation</h2>
					
					<h3>FUTUR PROJETS</h3>
					<ul>
						<li>
						
							<a href="projects.html"><img src="images/project-image1.jpg" alt=""></a>
						</li>
						<li>
							<a href="projects.html"><img src="images/project-image2.jpg" alt=""></a>
						</li>
						<li>
							<a href="projects.html"><img src="images/project-image3.jpg" alt=""></a>
						</li>
						<li>
							<a href="projects.html"><img src="images/project-image4.jpg" alt=""></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="body">
				<div>
					<h2 class="styleH23">A propos</h2>
					<p>Journale a propos de l'espace , des missions spacial , des nouvelles découvertes ,des nouvelles observations et des tendance dans le monde scientifique .....</p>
				</div>
			</div>
			<div class="footer">
				<div>
					<ul>
						<li>
							<h2 class="styleH22">PHOTO DU JOURS</h2>
							<a href="page/<?php echo $recentPost[0][8]; ?>-<?php echo $recentPost[0][0]; ?>.html"><img src="images/<?php echo $recentPost[0][4]; ?>" style="width:400px;height:250px;" alt=""></a>
						</li>
						<li>
							<h2 class="styleH22">Recement Poster</h2>
							<ul>
								<?php for($i=1;$i<2;$i++) { ?>
								<li>
									<a href="page/<?php echo $recentPost[$i][8]; ?>-<?php echo $recentPost[$i][0]; ?>.html"><img src="images/<?php echo $recentPost[$i][4]; ?>" style="width:100px;" alt=""></a>
									<h2 class="styleH2"><?php echo $recentPost[$i][3]; ?></h2>
									<span><?php echo $recentPost[$i][7]; ?></span>
									<a href="page/<?php echo $recentPost[$i][8]; ?>-<?php echo $recentPost[$i][0]; ?>.html" class="more">Voir</a>
								</li>
								<?php } ?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<?php include('page/footer.php'); ?>
	</div>
</body>
</html>